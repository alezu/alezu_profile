core = 7.x
api = 2

defaults[projects][subdir] = "contrib"
defaults[projects][l10n_url] = "http://ftp.drupal.org/files/translations/l10n_server.xml"

projects[api][type] = "module"
projects[api][subdir] = "custom"
projects[api][download][type] = "git"
projects[api][download][url] = "https://alezu@bitbucket.org/alezu/api.git"

projects[yamap][type] = "module"
projects[yamap][subdir] = "custom"
projects[yamap][download][type] = "git"
projects[yamap][download][url] = "https://alezu@bitbucket.org/alezu/yamap.git"

projects[vasp][type] = "module"
projects[vasp][download][subdir] = "custom"
projects[vasp][download][type] = "git"
projects[vasp][download][url] = "https://alezu@bitbucket.org/alezu/vasp.git"

projects[admin_menu][version] = ""
projects[bean][version] = ""
projects[ctools][version] = ""
projects[colorbox][version] = ""
projects[optimizedb][version] = ""
projects[devel][version] = ""
projects[email][version] = ""
projects[entity][version] = ""
projects[extlink][version] = ""
projects[favicon][version] = ""
projects[globalredirect][version] = ""
projects[htmlmail][version] = ""
projects[imce][version] = ""
projects[imce_wysiwyg][version] = ""
projects[jquery_update][version] = ""
projects[l10n_update][version] = ""
projects[libraries][version] = ""
projects[mailsystem][version] = ""
projects[menu_position][version] = ""
projects[metatag][version] = ""
projects[module_filter][version] = ""
projects[multiupload_filefield_widget][version] = ""
projects[multiupload_imagefield_widget][version] = ""
projects[pathauto][version] = ""
projects[token][version] = ""
projects[transliteration][version] = ""
projects[views][version] = ""
projects[webform][version] = ""
projects[webform_ajax][version] = ""
projects[wysiwyg][version] = "2.x-dev"
projects[xmlsitemap][version] = ""
projects[ocupload][version] = ""



libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.10/ckeditor_4.5.10_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"